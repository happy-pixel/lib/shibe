import { h, render } from 'preact'
import { Video as VideoComponent } from './components/index.js'

class VideoComponentClass {
  constructor (props) {
    this.props = props
    this.parent = document.querySelector(props.target)
    this.node = render(<VideoComponent {...this.props} />, this.parent)
    this.video = this.node.querySelector('video')
  }

  update (props) {
    this.props = Object.assign(this.props, props)
    render(<VideoComponent {...this.props} />, this.parent, this.node)
  }

  play () {
    this.video.play()
  }

  pause () {
    this.video.pause()
  }
}

if ('shibe' in window === false) {
  window.shibe = {
    Video: VideoComponentClass
  }
}
