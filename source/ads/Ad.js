import { Component } from 'preact'

/**
 * bazowa klasa do dalszego tworzenia okreslonych formatów
 * @class Ad
 */
export class Ad extends Component {
  constructor (props, state) {
    super(props, state)

    /**
     * tablica przechowująca klasy css dla głównego elementu komponentu
     * @type {String[]}
     */
    this.cssClasses = []
    this.inlineCss = {}

    if (!('style' in this.props)) this.props.style = {}

    this.state.lifecycle = 'initialized'

    /**
     * liczba zasobów aktualnie preloadowanych przez komponent
     * @type {number}
     */
    this.state.preloading = 0

    this.onReady = this.onReady.bind(this)
    this.classNames = this.classNames.bind(this)
    this.getInlineCss = this.getInlineCss.bind(this)
  }

  /**
   * callback wywoływany, gdy komponent zamontował się poprawnie i wszystkie zasoby zostały załadowane
   */
  onReady () {
    if (this.state.lifecycle !== 'ready') {
      this.setState({ lifecycle: 'ready' })
      if (typeof this.props.onReady === 'function') this.props.onReady()
    }
  }

  /**
   * zwraca bieżący komplet klas dla głównego pojemnika
   * @return {string}
   */
  classNames () {
    return (this.cssClasses.concat(this.props.classNames)).explode(', ')
  }

  /**
   * zwraca obiekt ze stylami
   */
  getInlineCss () {
    if ('style' in this.props && typeof this.props.style === 'object') {
      return { ...this.props.style, ...this.inlineCss }
    } else {
      return this.inlineCss
    }
  }

  render () {
    return (
      <div className={this.classNames()} style={this.props.style}></div>
    )
  }
}
