import style from './style.scss'
import { Ad } from '../Ad'

let classNames = (require('classnames/bind')).bind(style)

/**
 * za banner uważamy klikalny komponent o niepływającym (fixed, sticky, absolute) position
 * komponent jest traktowany zarówno bezpośrednio jako reklama, jak i jako komponent w innych formach reklamowych (np. interstitial)
 * @class Banner
 * @extends {Ad}
 */
export class Banner extends Ad {
  constructor (props, state) {
    super(props, state)

    /**
      * IntersectionObserver weryfikujacy obecność pojemnika w wiewporcie
      * @see https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API
      * @see https://developers.google.com/web/updates/2019/02/intersectionobserver-v2
      */
    this.observer = null

    /** domyślnie zakładamy, że element jest niewidoczny w viewporcie; stan ten może ulec zmianie dopiero po aktywacji IntersectionObservera */
    this.state.viewport = 'hidden'

    this.cssClasses = ['banner']

    if (!('redirectLayer' in this.props)) this.props.redirectLayer = false

    // @see https://reactjs.org/docs/components-and-props.html#es6-classes
    // @see https://medium.com/shoutem/react-to-bind-or-not-to-bind-7bf58327e22a
    this.classNames = this.classNames.bind(this)
    this.onReady = this.onReady.bind(this)
    this.mountIntersectionObserver = this.mountIntersectionObserver.bind(this)
    this.onViewportEnter = this.onViewportEnter.bind(this)
    this.onViewportLeave = this.onViewportLeave.bind(this)
  }

  /**
 * ustawienie styli które mają zostać dodane inline
 */
  getInlineCss () {
    if ('width' in this.props) this.inlineCss.width = this.inlineCss.maxWidth = this.props.width /** ustawia max-width, by umożliwić skalowanie */
    if ('height' in this.props) this.inlineCss.height = this.props.height
    return super.getInlineCss()
  }

  classNames () {
    return classNames([
      this.cssClasses,
      this.props.className,
      {
        visible: this.state.viewport !== 'hidden' && this.state.lifecycle === 'ready' && this.state.preloading === 0
      }
    ])
  }

  /**
   * inicjalizuje obiekt IntersectionObserver przypisany do głównego pojemnika komponentu
   */
  mountIntersectionObserver () {
    this.observer = new IntersectionObserver(
      changes => {
        for (const change of changes) {
          if (change.isIntersecting) {
            this.observer.visibleSince = change.time
            this.observer.wasVisible = true
            this.onViewportEnter()
          } else if (this.observer.wasVisible) {
            this.onViewportLeave()
          } else {
            this.observer.visibleSince = 0
          }
        }
      },
      {
        root: null,
        rootMargin: '0px',
        threshold: 0.5,
        trackVisibility: false,
        delay: 100
      }
    )

    /** aktywuje obiekt IntersectionObserver, przypisując go bezpośrednio do komponentu */
    this.observer.observe(this.base)

    /**
     * determinuje, czy element był kiedykolwiek widoczny; wykorzystywane, by uruchamiać funkcję onViewportLeave
     * @type {bool}
     */
    this.observer.wasVisible = false

    /**
     * czas w milisekundach, jaki element spędził w viewporcie
     * @type {number}
     */
    this.observer.visibleSince = 0
  }

  /**
   * wejście w obszar viewportu, zgodnie z ustawieniami IntersectionObservera
   */
  onViewportEnter () {
    // console.log('enter')
    this.setState({ viewport: 'enter' })
    if (typeof this.props.onViewportEnter === 'function') this.props.onViewportEnter()
  }
  /**
   * wyjście z obszaru viewportu, zgodnie z ustawieniami IntersectionObservera
   */
  onViewportLeave () {
    // console.log('leave')
    this.setState({ viewport: 'leave' })
    if (typeof this.props.onViewportLeave === 'function') this.props.onViewportLeave()
  }

  componentDidMount () {
    this.mountIntersectionObserver()
  }
}
