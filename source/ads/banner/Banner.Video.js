import style from './style.scss'
import { h } from 'preact'
import { Banner } from './Banner.js'
import { Link, Image, Video } from 'components'

let classNames = (require('classnames/bind')).bind(style)

/**
 * video odtwarzane po wejściu w obszar viewportu;<br>
 * po zakończeniu filmu wyświetlana jest klikalna grafika
 * @example
 * <BannerVideo
 *   width="300px"
 *   height="250px"
 *   url="http://example.com"
 *   img="http://placekitten.com/300/250"
 *   mp4="https://sample-videos.com/video123/mp4/240/big_buck_bunny_240p_10mb.mp4"
 *   autoplay
 *   vast=[
 *   {
 *     'firstQuartile': 'http://example.com/firstQuartile.gif',
 *     'midpoint': 'http://example.com/midpoint.gif',
 *     'thirdQuartile': 'http://example.com/thirdQuartile.gif'
 *   },
 *   {
 *     'firstQuartile': 'http://another.example.com/firstQuartile.gif',
 *     'midpoint': 'http://another.example.com/midpoint.gif',
 *     'thirdQuartile': 'http://another.example.com/thirdQuartile.gif'
 *   }
 *   ]
 *   style={
 *    'border': '3px dotted red'
 *   }
 *   onReady=function(){}
 *   onViewportEnter=function(){}
 *   onViewportLeave=function(){}
 * />
 *
 * @export
 * @class BannerVideo
 * @extends {Banner}
 *
 * @prop {object} props - zestaw właściwości komponentu
 * @prop {string} props.img - adres grafiki wyświetlanej po zakończeniu odtwarzania video
 * @prop {string} props.url - adres, na jaki ma przekierowywać grafika wyświetlana po zakończeniu video
 * @prop {string} props.mp4 - video w formacie mp4
 * @prop {string} [props.webm] - video w formacie webm
 * @prop {string} [props.ogv] - video w formacie ogv
 * @prop {object} [props.style] - obiekt ze stylami inline
 * @prop {string[]} [props.classNames] - dodatkowe klasy dla głównego elementu
 * @prop {string} props.width - szerokość komponentu wraz z jednostką
 * @prop {string} props.height - wysokość komponentu wraz z jednostką
 * @prop {boolean} [props.autoplay=true] - czy video ma zostać autoodtworzone po wejściu do viewportu
 * @prop {array|object[]} [props.vast] - zestaw(y) kodów mierzących zdarzenia video według specyfikacji vast
 * @prop {function} [props.onReady] - callback wywoływany, gdy komponent zamontował się poprawnie i wszystkie zasoby zostały załadowane
 * @prop {function} [props.onViewportEnter] - callback wywoływany, gdy komponent pojawił się w obszarze viewportu
 * @prop {function} [props.onViewportLeave] - callback wywoływany, gdy komponent opuścił obszar viewportu
 * @prop {object} state - zestaw stanów komponentu
 * @prop {string} state.lifecycle - string reprezentujący bieżący cykl życia komponentu (initialized, ready)
 * @prop {number} state.preloading - liczba zasobów aktualnie preloadowanych przez komponent
 */
export class BannerVideo extends Banner {
  constructor (props, state) {
    super(props, state)

    /**
      * referencja do komponentu &lt;Video/&gt; umożliwiająca korzystanie z jego wewnętrznych metod
      * @type {Component}
      */
    this.video = null

    /**
      * po jakim czasie od wejścia do viewportu uruchomić odtwarzanie video
      * @type {number}
      */
    this.viewability = 500

    /**
      * przechowuje timeout rozpoczynający odtwarzanie video po wejściu do viewportu
      */
    this.videoAutoplayTimeout = null

    this.cssClasses = [...this.cssClasses, 'video']

    this.onVideoEnd = this.onVideoEnd.bind(this)
    this.replay = this.replay.bind(this)
    this.onClose = this.onClose.bind(this)
  }

  /**
   * wejście w obszar viewportu;<br>
   * ustawia dodatkowo timeout, po którym video zostaje autoodtworzone, jeśli prop autoplay został ustawiony na true
   */
  onViewportEnter () {
    if (!this.state.videoEnded) {
      this.videoAutoplayTimeout = setTimeout(() => {
        if (!this.state.autoplay) {
          this.setState({ autoplay: true })
        }
        this.video.play()
        this.videoAutoplayTimeout = null
      }, this.viewability)
    }
    super.onViewportEnter()
  }

  /**
   * wyjście z obszaru viewportu;<br>
   * pauzuje odtwarzanie video, jeśli było uruchomione
   */
  onViewportLeave () {
    if (!this.state.videoEnded) {
      if (this.videoAutoplayTimeout) {
        clearTimeout(this.videoAutoplayTimeout)
      }
      this.video.pause()
    }
    super.onViewportLeave()
  }

  /**
   * callback wywoływany przez komponent &lt;Video/&gt;, zmiana videoEnded na true ukrywa video i pokazuje w jego miejsce klikalną grafikę
   */
  onVideoEnd () {
    this.setState({ videoEnded: true })
  }

  /**
   * wykorzystywany przez przycisk wyświetlany nad klikalną grafiką
   */
  replay () {
    this.setState({ videoEnded: false })
    this.video.play()
  }

  /**
   * obsługa zamykania (przekazywane przez parent component)
   */
  onClose () {
    this.video.onClose()
  }

  render () {
    const { autoplay, mp4, webm, ogv, vast, aspectRatio, url, img } = this.props

    return (
      <div
        ref={c => {
          this.banner = c
        }}
        className={this.classNames()} style={this.getInlineCss()}>
        <Video
          ref={video => { this.video = video }}
          autoplay={autoplay}
          playsinline
          gui={['mute', 'fullscreen']}
          mp4={mp4}
          webm={webm}
          ogv={ogv}
          onReady={this.onReady}
          onEnded={this.onVideoEnd}
          vast={vast}
          aspectRatio={aspectRatio}>
            <Link href={url} className={classNames('clickLayer')} target="_blank"></Link>
          </Video>
        {this.state.videoEnded && (
          <div className={classNames('videoEnded')}>
            {img && <Link href={url} target="_blank">
              <Image src={img} />)
            </Link>}
            <button onClick={this.replay}>
              <svg viewBox="0 0 512 512">
                <path d="M0 436V76c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v151.9L235.5 71.4C256.1 54.3 288 68.6 288 96v131.9L459.5 71.4C480.1 54.3 512 68.6 512 96v320c0 27.4-31.9 41.7-52.5 24.6L288 285.3V416c0 27.4-31.9 41.7-52.5 24.6L64 285.3V436c0 6.6-5.4 12-12 12H12c-6.6 0-12-5.4-12-12z">
                </path>
              </svg>
            </button>
          </div>
        )}
      </div>
    )
  }
}
