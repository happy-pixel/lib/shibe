import { h } from 'preact'
import { Banner } from './Banner.js'
import { Link, Iframe } from 'components/index.js'

/**
 * iframe nad którym opcjonalnie jest umieszczona klikalna warstwa
 * @class BannerIframe
 * @extends Banner
 * @example
 * <BannerIframe
 *   width="300px"
 *   height="250px"
 *   url="http://example.com"
 *   iframe="http://example.com/index.html"
 *   redirectLayer
 *   package={
 *     'event1': 'http://example.com/event1.gif',
 *     'event2': 'http://example.com/event2.gif',
 *     'event3': 'http://example.com/event3.gif'
 *   }
 * />
 *
 * @prop {object} props - zestaw właściwości komponentu
 * @prop {string} props.iframe - adres wyświetlanej strony
 * @prop {string} props.url - adres, na jaki ma przekierowywać reklama
 * @prop {object} [props.style] - obiekt ze stylami inline
 * @prop {string[]} [props.classNames] - dodatkowe klasy dla głównego elementu
 * @prop {string} props.width - szerokość komponentu wraz z jednostką
 * @prop {string} props.height - wysokość komponentu wraz z jednostką
 * @prop {object} [props.package={}] - obiekt z danymi wysyłanymi do iframe za pomocą postmessage
 * @prop {boolean} [props.redirectLayer=false] - decyduje, czy wyświetlić nad elementem wizualnym reklamy (grafika, video, iframe) transparentny link przechwytujący kliknięcia
 * @prop {function} [props.onReady] - callback wywoływany, gdy komponent zamontował się poprawnie i wszystkie zasoby zostały załadowane
 * @prop {function} [props.onViewportEnter] - callback wywoływany, gdy komponent pojawił się w obszarze viewportu
 * @prop {function} [props.onViewportLeave] - callback wywoływany, gdy komponent opuścił obszar viewportu
 * @prop {object} state - zestaw stanów komponentu
 * @prop {string} state.lifecycle - string reprezentujący bieżący cykl życia komponentu (initialized, ready)
 * @prop {number} state.preloading - liczba zasobów aktualnie preloadowanych przez komponent
 */
export class BannerIframe extends Banner {
  constructor (props, state) {
    super(props, state)
    /**
      * referencja do komponentu &lt;Iframe/&gt; umożliwiająca korzystanie z jego wewnętrznych metod
      * @type {Component}
      */
    this.iframe = null
  }

  render () {
    return (
      <div className={this.classNames()} style={this.getInlineCss()}>
        <Iframe
          ref={ref => { this.iframe = ref }}
          src={this.props.iframe}
          package={this.props.package}
          onShow={this.onReady}
        />
        {this.props.redirectLayer && <Link href={this.props.url} className="layer" target="_blank" />}
      </div>
    )
  }
}
