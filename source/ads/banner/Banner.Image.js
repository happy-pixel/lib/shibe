import { h } from 'preact'
import { Banner } from './Banner.js'
import { Link, Image } from 'components/index.js'

/**
 * pojedyncza grafika przekierowująca na adres po kliknięciu
 *
 * @export
 * @class BannerImage
 * @extends {Banner}
 * @example
 * <BannerImage
 *   width="300px"
 *   height="250px"
 *   url="http://example.com"
 *   img="http://placekitten.com/300/250"
 * />
 *
 * @prop {object} props - zestaw właściwości komponentu
 * @prop {string} props.img - adres wyświetlanej grafiki
 * @prop {string} props.url - adres, na jaki ma przekierowywać reklama
 * @prop {'cover'|'contain'} [props.fill=cover] - sposób wypełnienia obszaru reklamy przez grafikę, jeżeli te się różnią proporcjami
 * @prop {object} [props.style] - obiekt ze stylami inline
 * @prop {string[]} [props.classNames] - dodatkowe klasy dla głównego elementu
 * @prop {string} props.width - szerokość komponentu wraz z jednostką
 * @prop {string} props.height - wysokość komponentu wraz z jednostką
 * @prop {function} [props.onReady] - callback wywoływany, gdy komponent zamontował się poprawnie i wszystkie zasoby zostały załadowane
 * @prop {function} [props.onViewportEnter] - callback wywoływany, gdy komponent pojawił się w obszarze viewportu
 * @prop {function} [props.onViewportLeave] - callback wywoływany, gdy komponent opuścił obszar viewportu
 * @prop {object} state - zestaw stanów komponentu
 * @prop {string} state.lifecycle - string reprezentujący bieżący cykl życia komponentu (initialized, ready)
 * @prop {number} state.preloading - liczba zasobów aktualnie preloadowanych przez komponent
 */
export class BannerImage extends Banner {
  render () {
    return (
      <div className={this.classNames()} style={this.getInlineCss()}>
        <Link href={this.props.url} className="fullsize" target="_blank">
          <Image
            fill={this.props.fill}
            src={this.props.img}
            onReady={this.onReady}
          />
        </Link>
      </div>
    )
  }
}
