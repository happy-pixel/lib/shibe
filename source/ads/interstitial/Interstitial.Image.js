import style from './style.scss'
import { h } from 'preact'
import { Interstitial } from './Interstitial'
import { BannerImage } from 'ads/banner'

let classNames = (require('classnames/bind')).bind(style)
/**
 * iframe nad którym opcjonalnie jest umieszczona klikalna warstwa <br>
 * każda z orientacji ekranu może otrzymać swój własny iframe
 * @example
 * <InterstitialImage
 *   url="http://example.com"
 *   portrait="http://example.com/portrait.jpg"
 *   landscape="http://example.com/landscape.jpg"
 * />
 *
 * @export
 * @class InterstitialImage
 * @extends {Interstitial}
 *
 * @prop {object} props - zestaw właściwości komponentu
 * @prop {string} [props.portrait] - adres grafiki wyświetlanej w widoku portrait
 * @prop {string} [props.landscape] - adres grafiki wyświetlanej w widoku landscape
 * @prop {string} props.url - adres, na jaki ma przekierowywać reklama
 * @prop {function} [props.onReady] - callback wywoływany, gdy komponent zamontował się poprawnie i wszystkie zasoby zostały załadowane
 * @prop {object} state - zestaw stanów komponentu
 * @prop {string} state.lifecycle - string reprezentujący bieżący cykl życia komponentu (initialized, ready)
 * @prop {number} state.preloading - liczba zasobów aktualnie preloadowanych przez komponent
 */
export class InterstitialImage extends Interstitial {
  body () {
    return (
      <div className={classNames(
        'body',
        {
          'landscape': this.displayOnOrientation === 'landscape',
          'portrait': this.displayOnOrientation === 'portrait'
        })}>
        {this.displayOnOrientation !== 'portrait' &&
        <div className={classNames('landscape')}>
          <BannerImage
            url={this.props.url}
            width="100%"
            height="100%"
            fill="cover"
            img={this.props.landscape}
            onReady={this.onReadyLandscape}
          />
        </div>}
        {this.displayOnOrientation !== 'landscape' &&
        <div className={classNames('portrait')}>
          <BannerImage
            url={this.props.url}
            width="100%"
            height="100%"
            fill="cover"
            img={this.props.portrait}
            onReady={this.onReadyPortrait}
          />
        </div>}
      </div>)
  }
}
