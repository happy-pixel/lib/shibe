import style from './style.scss'
import { h } from 'preact'
import { Interstitial } from './Interstitial'
import { BannerVideo, BannerImage } from 'ads/banner'

let classNames = (require('classnames/bind')).bind(style)
/**
 * video wyświetlane w połowie obszaru reklamy
 * dla portrait na max 100% szerokości, wysokość auto
 * dla landscape na max 100% wysokości, szerokośc auto
 * pozostały obszar wypełnia klikalna grafika tła
 * po zakończeniu filmu w jego miejsce wyświetlana dodatkowa, klikalna grafika
 * @example
 * <InterstitialVideo
 *   url="http://example.com"
 *   poster="http://placekitten.com/300/250"
 *   mp4="https://sample-videos.com/video123/mp4/240/big_buck_bunny_240p_10mb.mp4"
 *   webm="https://sample-videos.com/video123/webm/240/big_buck_bunny_240p_10mb.webm"
 *   ogv="https://sample-videos.com/video123/ogv/240/big_buck_bunny_240p_10mb.ogv"
 *   autoplay
 *   vast=[
  *   {
  *     'firstQuartile': 'http://example.com/firstQuartile.gif',
  *     'midpoint': 'http://example.com/midpoint.gif',
  *     'thirdQuartile': 'http://example.com/thirdQuartile.gif'
  *   },
  *   {
  *     'firstQuartile': 'http://another.example.com/firstQuartile.gif',
  *     'midpoint': 'http://another.example.com/midpoint.gif',
  *     'thirdQuartile': 'http://another.example.com/thirdQuartile.gif'
  *   }
  *   ]
 *   onReady=function(){}
 * />
 *
 * @export
 * @class InterstitialVideo
 * @extends {Interstitial}
 *
 * @prop {object} props - zestaw właściwości komponentu
 * @prop {string} props.poster - adres grafiki wyświetlanej w miejscu video po jego zakończeniu odtwarzania
 * @prop {string} [props.portrait] - adres grafiki wyświetlanej w widoku portrait
 * @prop {string} [props.landscape] - adres grafiki wyświetlanej w widoku landscape
 * @prop {string} props.url - adres, na jaki ma przekierowywać reklama
 * @prop {boolean} [props.autoplay=true] - czy video ma zostać autoodtworzone pojawieniu się reklamy
 * @prop {array|object[]} [props.vast] - zestaw(y) kodów mierzących zdarzenia video według specyfikacji vast
 * @prop {function} [props.onReady] - callback wywoływany, gdy komponent zamontował się poprawnie i wszystkie zasoby zostały załadowane
 * @prop {object} state - zestaw stanów komponentu
 * @prop {string} state.lifecycle - string reprezentujący bieżący cykl życia komponentu (initialized, ready)
 * @prop {number} state.preloading - liczba zasobów aktualnie preloadowanych przez komponent
 */
export class InterstitialVideo extends Interstitial {
  constructor (props, state) {
    super(props, state)
    /**
      * referencja do komponentu &lt;BannerVideo/&gt; umożliwiająca korzystanie z jego wewnętrznych metod
      * @type {Component}
      */
    this.banner = null
  }

  handleClick () {
    this.banner.onClose()
    super.handleClick()
  }

  body () {
    return (
      <div className={classNames('container', this.displayOnOrientation)}>
        <BannerVideo
          ref={banner => { this.banner = banner }}
          url={this.props.url}
          img={this.props.poster}
          mp4={this.props.mp4}
          webm={this.props.webm}
          ogv={this.props.ogv}
          autoplay={this.props.autoplay}
          aspectRatio={this.props.aspectRatio}
          vast={this.props.vast}
          width="100%"
          height="100%"
          onReady={this.onReady}
        />
        <div className={classNames('background', 'body')}>
          {this.displayOnOrientation !== 'portrait' &&
          <div className={classNames('landscape')}>
            <BannerImage
              url={this.props.url}
              width="100%"
              height="100%"
              img={this.props.landscape}
              onReady={this.onReadyLandscape}
            />
          </div>}
          {this.displayOnOrientation !== 'landscape' &&
          <div className={classNames('portrait')}>
            <BannerImage
              url={this.props.url}
              width="100%"
              height="100%"
              img={this.props.portrait}
              onReady={this.onReadyPortrait}
            />
          </div>}
        </div>
      </div>)
  }
}
