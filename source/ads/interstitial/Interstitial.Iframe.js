import style from './style.scss'
import { h } from 'preact'
import { Interstitial } from './Interstitial'
import { BannerIframe } from 'ads/banner'

let classNames = (require('classnames/bind')).bind(style)
/**
 * iframe nad którym opcjonalnie jest umieszczona klikalna warstwa <br>
 * każda z orientacji ekranu może otrzymać swój własny iframe
 * @example
 * <InterstitialIframe
 *   url="http://example.com"
 *   portrait="http://example.com/portrait.html"
 *   landscape="http://example.com/landscape.html"
 *   redirectLayer
 *   package={
 *     'event1': 'http://example.com/event1.gif',
 *     'event2': 'http://example.com/event2.gif',
 *     'event3': 'http://example.com/event3.gif'
 *   }
 * />
 *
 * @export
 * @class InterstitialIframe
 * @extends {Interstitial}
 *
 * @prop {object} props - zestaw właściwości komponentu
 * @prop {string} [props.portrait] - adres wyświetlanej strony w widoku portrait
 * @prop {string} [props.landscape] - adres wyświetlanej strony w widoku landscape
 * @prop {string} props.url - adres, na jaki ma przekierowywać reklama
 * @prop {object} [props.package={}] - obiekt z danymi wysyłanymi do iframe za pomocą postmessage
 * @prop {boolean} [props.redirectLayer=false] - decyduje, czy wyświetlić nad elementem wizualnym reklamy (grafika, video, iframe) transparentny link przechwytujący kliknięcia
 * @prop {function} [props.onReady] - callback wywoływany, gdy komponent zamontował się poprawnie i wszystkie zasoby zostały załadowane
 * @prop {object} state - zestaw stanów komponentu
 * @prop {string} state.lifecycle - string reprezentujący bieżący cykl życia komponentu (initialized, ready)
 * @prop {number} state.preloading - liczba zasobów aktualnie preloadowanych przez komponent
 */
export class InterstitialIframe extends Interstitial {
  constructor (props, state) {
    super(props, state)
    if (!('redirectLayer' in this.props)) this.props.redirectLayer = false
  }

  body () {
    return (
      <div className={classNames(
        'body',
        {
          'landscape': this.displayOnOrientation === 'landscape',
          'portrait': this.displayOnOrientation === 'portrait'
        })}>
        {this.displayOnOrientation !== 'portrait' &&
          <div className={classNames('landscape')}>
            <BannerIframe
              url={this.props.url}
              width="100%"
              height="100%"
              redirectLayer={this.props.redirectLayer}
              iframe={this.props.landscape}
              package={this.props.package}
              onReady={this.onReadyLandscape}
            />
          </div>}
        {this.displayOnOrientation !== 'landscape' &&
          <div className={classNames('portrait')}>
            <BannerIframe
              url={this.props.url}
              width="100%"
              height="100%"
              redirectLayer={this.props.redirectLayer}
              iframe={this.props.portrait}
              package={this.props.package}
              onReady={this.onReadyPortrait}
            />
          </div>}
      </div>)
  }
}
