import style from './style.scss'
import { h } from 'preact'
import { Ad } from '../Ad'
import { Header } from './Header'
import { ChangeOrientationMessage } from './ChangeOrientationMessage'

let classNames = (require('classnames/bind')).bind(style)
/**
 * wyświetlana na warstwie reklama, wypełniająca cały viewport <br>
 * obowiązkowo posiada belkę w górnej częsci, zawierającą przycisk zamykający oraz napis reklama
 *
 * @export
 * @class Interstitial
 * @extends {Ad}
 */
export class Interstitial extends Ad {
  constructor (props, state) {
    super(props, state)

    /**
     * na jakiej orientacji ekranu ma byc być wyświetlana treść reklamowa
     * @type {{'both','portrait','landscape'}}
     */
    this.displayOnOrientation = null

    if ('landscape' in this.props && 'portrait' in this.props) {
      this.displayOnOrientation = 'both'
    } else if ('portrait' in this.props) {
      this.displayOnOrientation = 'portrait'
    } else if ('landscape' in this.props) {
      this.displayOnOrientation = 'landscape'
    }

    this.state.readyLandscape = (this.displayOnOrientation === 'portrait')
    this.state.readyPortrait = (this.displayOnOrientation === 'landscape')
    this.state.width = 0
    this.state.height = 0
    this.state.orientation = null

    this.body = this.body.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.updateOrientation = this.updateOrientation.bind(this)
    this.onReadyLandscape = this.onReadyLandscape.bind(this)
    this.onReadyPortrait = this.onReadyPortrait.bind(this)
  }

  componentDidMount () {
    this.updateOrientation()
    window.addEventListener('resize', this.updateOrientation)
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.updateOrientation)
  }

  /**
 * obsługa zamykania po kliknięciu
 */
  handleClick () {
    this.setState({ lifecycle: 'closed' })
    window.removeEventListener('resize', this.updateOrientation)
  }

  /**
 * ustawienie orientacji portrait lub landscape na podstawie wymiarów urządzenia
 */
  updateOrientation () {
    this.setState({ width: window.innerWidth, height: window.innerHeight })
    let orientation = (this.state.height >= this.state.width) ? 'portrait' : 'landscape'
    this.setState({ orientation: orientation })
  }

  /**
 * wywołanie onReady dopiero po załadowaniu elementów dla bieżącej orientacji
 */
  onReady () {
    let landscape = (this.state.readyLandscape && this.state.orientation === 'landscape')
    let portrait = (this.state.readyPortrait && this.state.orientation === 'portrait')
    let both = (this.state.readyLandscape && this.state.readyPortrait)
    if (landscape || portrait || both) {
      super.onReady()
    }
  }

  /**
 * obsługa zaladowania elementu landscape
 */
  onReadyLandscape () {
    this.setState({ readyLandscape: true })
    this.onReady()
  }

  /**
 * obsługa zaladowania elementu portrait
 */
  onReadyPortrait () {
    this.setState({ readyPortrait: true })
    this.onReady()
  }

  /**
   * treść reklamy, wyświetlana pod belką
   *
   * @returns {jsx}
   * @memberof Interstitial
   */
  body () {
    return null
  }

  render () {
    let changeOrientation = false
    if (this.displayOnOrientation === 'portrait') changeOrientation = 'landscape'
    if (this.displayOnOrientation === 'landscape') changeOrientation = 'portrait'

    return (
      <div style={this.props.style} className={classNames({
        interstitial: true,
        ready: this.state.lifecycle === 'ready' && this.state.preloading === 0
      })}>
        <Header
          onClick={this.handleClick}
        />
        <div className={classNames('container')}>
          {changeOrientation && <ChangeOrientationMessage displayOn={changeOrientation} />}
          {this.body()}
        </div>
      </div>
    )
  }
}
