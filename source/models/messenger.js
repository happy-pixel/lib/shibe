export class Messenger {
  constructor (config = {}) {
    this.source = {
      node: config.source.node,
      origin: new URL(config.source.origin).origin
    }
    this.target = {
      node: config.target.node,
      origin: new URL(config.target.origin).origin
    }

    this.events = {}
    this.source.node.addEventListener(
      'message',
      data => this.listener(data),
      false
    )
  }

  send (event = 'undefined', data = {}) {
    this.target.node.postMessage(
      {
        event: event,
        data
      },
      this.target.origin
    )
  }

  on (eventName = 'undefined', callback = function () {}) {
    this.events[eventName] = callback
  }

  listener (message = 'undefined') {
    if (message.data.event in this.events) {
      this.events[message.data.event].call(this, message.data.value)
    }
  }
}
