export const log = (...args) =>
  process.env.NODE_ENV !== 'production' ? console.log(...args) : null
export const error = (...args) =>
  process.env.NODE_ENV !== 'production' ? console.error(...args) : null
export const info = (...args) =>
  process.env.NODE_ENV !== 'production' ? console.info(...args) : null
