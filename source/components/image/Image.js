import style from './style.scss'
import { h, Component } from 'preact'

let classNames = (require('classnames/bind')).bind(style)

/**
 * prosty element img
 * @class Image
 * @extends {Component}
 * @example
 * <Image
 *   width="300px"
 *   height="250px"
 *   src="http://placekitten.com/300/250"
 *   style={
 *    'border': '3px dotted red'
 *   }
 *   onReady=function(){}
 * />
 *
 * @prop {object} props - zestaw właściwości komponentu
 * @prop {string} props.src - adres grafiki
 * @prop {string} [props.width] - szerokość komponentu wraz z jednostką
 * @prop {string} [props.height] - wysokość komponentu wraz z jednostką
 * @prop {string} [props.objectFit=contain] - @see https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit
 * @prop {string} [props.validate=never] - @see https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XUL/image#a-validate
 * @prop {object} [props.style] - obiekt ze stylami inline
 * @prop {function} [props.onReady] - callback wywoływany, gdy obrazek zamontował się poprawnie
 */
export class Image extends Component {
  constructor (props, state) {
    super(props, state)

    if (!('style' in this.props)) this.props.style = {}
    if (!('objectFit' in this.props.style)) this.props.style.objectFit = ('objectFit' in this.props) ? this.props.objectFit : 'contain'
    if (!('validate' in this.props)) this.props.validate = 'never'

    this.handleOnLoad = this.handleOnLoad.bind(this)
    this.getInlineCss = this.getInlineCss.bind(this)

    if (typeof this.props.onInit === 'function') this.props.onInit()
  }

  /**
   * obsługa eventu onLoad grafiki
   */
  handleOnLoad () {
    if (typeof this.props.onReady === 'function') this.props.onReady()
  }

  /**
   * ustawienie styli na bazie props.style lub props.fill
   */
  getInlineCss () {
    if ('style' in this.props && typeof this.props.style === 'object') {
      return this.props.style
    } else if ('fill' in this.props && typeof this.props.fill === 'string') {
      return { objectFit: this.props.fill }
    }
  }

  render () {
    return (
      <img
        width={ this.props.width }
        height={ this.props.height }
        className={ classNames('image') }
        style={ this.getInlineCss() }
        src={ this.props.src }
        validate={ this.props.validate }
        onLoad={ this.handleOnLoad }
      />
    )
  }
}
