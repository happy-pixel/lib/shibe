import style from './style.scss'
import { h, Component } from 'preact'
import PlayDuotone from 'source/assets/icons/play-duotone'

let classNames = (require('classnames/bind')).bind(style)

/**
 * podstawowy komponent video html5
 *
 * @export
 * @class Video
 * @extends {Component}
 */
export class Video extends Component {
  constructor (props, state) {
    super(props, state)

    /**
      * referencja do node video; umożliwiająca korzystanie z natywnych właściwości
      * @type {Node}
      */
    this.video = null

    this.state.percentile = 0
    this.state.quartile = 0
    this.state.seconds = 0
    this.state.muted = this.props.muted || this.props.autoplay || false

    if (!('gui' in this.props)) this.props.gui = []

    /**
      * vast może być zarówno pojedynczym obiektem, jak i tablicą z obiektami
      * poniższy kod ma na celu ujednolicenie postaci do tablicy
      */
    if ('vast' in this.props && typeof this.props.vast === 'object') {
      if (this.props.vast instanceof Array) {
        this.props.vast = this.props.vast
      } else {
        this.props.vast = [this.props.vast]
      }
    } else {
      this.props.vast = []
    }

    this.onInit = this.onInit.bind(this)
    this.onPlay = this.onPlay.bind(this)
    this.onClick = this.onClick.bind(this)
    this.onPause = this.onPause.bind(this)
    this.onStop = this.onStop.bind(this)
    this.onLoadedData = this.onLoadedData.bind(this)
    this.onEnded = this.onEnded.bind(this)
    this.onTimeUpdate = this.onTimeUpdate.bind(this)
    this.onWaiting = this.onWaiting.bind(this)
    this.onQuartileChange = this.onQuartileChange.bind(this)
    this.onFullSecondChange = this.onFullSecondChange.bind(this)
    this.onVastEvent = this.onVastEvent.bind(this)
    this.onClose = this.onClose.bind(this)

    this.toggle = this.toggle.bind(this)
    this.play = this.play.bind(this)
    this.pause = this.pause.bind(this)
    this.toggleVolume = this.toggleVolume.bind(this)
    this.toggleFullScreen = this.toggleFullScreen.bind(this)

    if (typeof this.props.onPlay === 'function') this.props.onPlay = this.props.onPlay.bind(this)
    if (typeof this.props.onClick === 'function') this.props.onClick = this.props.onClick.bind(this)
    if (typeof this.props.onPause === 'function') this.props.onPause = this.props.onPause.bind(this)
    if (typeof this.props.onStop === 'function') this.props.onStop = this.props.onStop.bind(this)
    if (typeof this.props.onReady === 'function') this.props.onReady = this.props.onReady.bind(this)
    if (typeof this.props.onEnded === 'function') this.props.onEnded = this.props.onEnded.bind(this)
    if (typeof this.props.onTimeUpdate === 'function') this.props.onTimeUpdate = this.props.onTimeUpdate.bind(this)
    if (typeof this.props.onWaiting === 'function') this.props.onWaiting = this.props.onWaiting.bind(this)
    if (typeof this.props.onQuartileChange === 'function') this.props.onQuartileChange = this.props.onQuartileChange.bind(this)
    if (typeof this.props.onFullSecondChange === 'function') this.props.onFullSecondChange = this.props.onFullSecondChange.bind(this)
    if (typeof this.props.onVastEvent === 'function') this.props.onVastEvent = this.props.onVastEvent.bind(this)

    this.onInit()
  }

  /**
    * callback na zakończenie działania constructora
    */
  onInit () {}

  /**
    * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/play_event
    */
  onPlay () {
    this.setState({ status: 'playing' })
    this.onVastEvent((this.state.percentile === 0) ? 'start' : 'resume')
    if (typeof this.props.onPlay === 'function') this.props.onPlay()
  }

  /**
    * zakończenie odtwarzania video również wywołuje event pause
    * w takim przypadku przerzucam event na onStop
    */
  onPause () {
    if (this.state.percentile < 100) {
      this.setState({ status: 'paused' })
      this.onVastEvent('pause')
      if (typeof this.props.onPause === 'function') this.props.onPause()
    } else {
      this.onStop()
    }
  }

  /**
    * wstrzymanie odtwarzania video
    */
  onStop () {
    this.setState({ status: 'stopped' })
    if (typeof this.props.onStop === 'function') this.props.onStop()
  }

  /**
    * wywołuje props.onReady(), jeżeli załadowana została wystarczająca ilość danych video
    * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/loadeddata_event
    * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/readyState
    */
  onLoadedData () {
    if (typeof this.props.onReady === 'function' && this.video.readyState >= 2) this.props.onReady()
  }

  /**
   * pełne zakończenie odtwarzania video
   */
  onEnded () {
    this.setState({ status: 'completed' })
    this.onVastEvent('complete')
    if (typeof this.props.onEnded === 'function') this.props.onEnded()
  }

  /**
    * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/timeupdate_event
    */
  onTimeUpdate () {
    /**
      * procent odtworzenia video
      * @type {number}
      */
    let percentile = Math.floor((this.video.currentTime / this.video.duration) * 100) / 100

    /**
      * numer ćwiartki video przed bieżącymi obliczeniami, wykorzystywany do weryfikacji, czy nastąpiła zmiana ćwiartki
      */
    let currentQuartile = this.state.quartile

    /**
      * sekunda odtwarzania przed bieżącymi obliczeniami, wykorzystywana do weryfikacji, czy nastąpiła zmiana pełnej sekundy odtwarzania video
      */
    let currentTime = this.state.seconds

    this.setState({ 
      percentile: percentile,
      seconds: Math.round(this.video.currentTime),
      quartile: Math.floor(percentile * 4)
    })

    if (currentQuartile !== this.state.quartile) this.onQuartileChange()
    if (currentTime !== this.state.seconds) this.onFullSecondChange()

    if (typeof this.props.onTimeUpdate === 'function') this.props.onTimeUpdate()
  }

  /**
    * wyłączenie obsługi eventu - wywoluje się natychmiast po onPlay przy ponownym odtworzeniu video
    * do sprawdzenia
    */
  onWaiting () {
    // this.setState({ status: 'waiting' })
    // if (typeof this.props.onWaiting === 'function') this.props.onWaiting()
  }

  /**
    * zmiana ćwiartki czasu odtwarzanego video
    * VAST określa zdarzenie zaraportowania odtworzenia pełnych 25% czasu odtwarzania video
    */
  onQuartileChange () {
    switch (this.state.quartile) {
      case 1:
        this.onVastEvent('firstQuartile')
        break
      case 2:
        this.onVastEvent('midpoint')
        break
      case 3:
        this.onVastEvent('thirdQuartile')
        break
    }
    if (typeof this.props.onQuartileChange === 'function') this.props.onQuartileChange(this.state.quartile)
  }

  /**
    * zmiana czasu odtwarzania video w postaci pełnych sekund
    * event onTimeUpdate jest wywoływany przy zmiennych interwałach, co może stanowić problem w niektórych scenariuszach
    */
  onFullSecondChange () {
    this.onVastEvent('videoplay')
    if (typeof this.props.onFullSecondChange === 'function') this.props.onFullSecondChange(this.state.seconds)
  }

  /**
    * jeżeli nie zostanie dostarczona dedykowana obsługa zdarzenia <br>
    * kliknięcie w obszar video pauzuje/wznawia odtwarzanie
    * w sytuacji w której zadeklarowano autoodtwarzanie video startuje ono bez dźwięki a kliknięcie włącza/wyłącza dźwięk
    */
  onClick () {
    if (typeof this.props.onClick === 'function') {
      this.props.onClick()
    } else if (this.props.autoplay) {
      this.toggleVolume()
    } else {
      this.toggle()
    }
  }

  /**
  * obsługa zamykania (przekazywane przez parent component)
  */
  onClose () {
    this.pause()
    this.onVastEvent('close')
    if (typeof this.props.onClose === 'function') this.props.onClose()
  }

  /**
    * zliczenie wszystkich kodów mierzących z wszystkich zestawów dla danego klucza
    * @param {string} eventName - klucz zdarzenia
    */
  onVastEvent (eventName) {
    /**
     * część dostarczonych kodów mierzących zawiera token, który ma zostać zastapiony bieżącym timestampem
     */
    let timestamp = Math.round((new Date().getTime()) / 1000)
    let token = new RegExp(/((\[|\{|\()*)timestamp((\]|\}|\))*)/gi)

    if ('vast' in this.props && typeof this.props.vast === 'object') {
      this.props.vast.forEach(set => {
        if (eventName in set) {
          /**
            * obsługujemy tylko kody mierzące w postaci urla bezwzględnego do grafiki
            */
          let image = new Image()
          let url = set[eventName]

          if ((token.test(url))) url.replace(token, timestamp)

          /**
            * dodatkowy wymóg adserwera adocean dla kodów mierzących video
            */
          if (url.includes('adocean')) url += `|_vtm:${this.state.seconds}/_${timestamp}`

          image.src = url
        }
      })
    }
    if (typeof this.props.onVastEvent === 'function') this.props.onVastEvent(eventName)
  }

  /**
    * toggle
    */
  toggle () {
    if (this.state.status === 'playing') {
      this.pause()
    } else {
      this.play()
    }
  }

  /**
    * play
    */
  play () {
    this.video.play().catch(() => {})
  }

  /**
    * pause
    */
  pause () {
    this.video.pause()
  }

  /**
    * mute
    */
  mute () {
    this.setState({ muted: true })
    this.onVastEvent('mute')
  }

  /**
    * unmute
    */
  unmute () {
    this.setState({ muted: false })
    this.onVastEvent('unmute')
  }

  toggleVolume () {
    if (this.state.muted) {
      this.unmute()
    } else {
      this.mute()
    }
  }

  toggleFullScreen () {
    if (this.video.requestFullScreen) {
      this.video.requestFullScreen()
    } else if (this.video.webkitRequestFullScreen) {
      this.video.webkitRequestFullScreen()
    } else if (this.video.mozRequestFullScreen) {
      this.video.mozRequestFullScreen()
    }
    this.onVastEvent('fullscreen')
  }

  render () {
    let aspectRatio = (this.props.aspectRatio) ? `aspectRatio-${this.props.aspectRatio.replace(':', 'x')}` : false

    return (
      <div className={classNames('videoComponent', aspectRatio, { 'aspectRatio': aspectRatio })}>
        {'gui' in this.props && this.props.gui.includes('mute') && (
          <button onClick={this.toggleVolume}>
            {this.state.muted && (
              <svg viewBox="0 0 512 512">
                <path d="M215.03 71.05L126.06 160H24c-13.26 0-24 10.74-24 24v144c0 13.25 10.74 24 24 24h102.06l88.97 88.95c15.03 15.03 40.97 4.47 40.97-16.97V88.02c0-21.46-25.96-31.98-40.97-16.97zM461.64 256l45.64-45.64c6.3-6.3 6.3-16.52 0-22.82l-22.82-22.82c-6.3-6.3-16.52-6.3-22.82 0L416 210.36l-45.64-45.64c-6.3-6.3-16.52-6.3-22.82 0l-22.82 22.82c-6.3 6.3-6.3 16.52 0 22.82L370.36 256l-45.63 45.63c-6.3 6.3-6.3 16.52 0 22.82l22.82 22.82c6.3 6.3 16.52 6.3 22.82 0L416 301.64l45.64 45.64c6.3 6.3 16.52 6.3 22.82 0l22.82-22.82c6.3-6.3 6.3-16.52 0-22.82L461.64 256z"></path>
              </svg>
            )}
            {!this.state.muted && (
              <svg viewBox="0 0 480 512">
                <path d="M215.03 71.05L126.06 160H24c-13.26 0-24 10.74-24 24v144c0 13.25 10.74 24 24 24h102.06l88.97 88.95c15.03 15.03 40.97 4.47 40.97-16.97V88.02c0-21.46-25.96-31.98-40.97-16.97zM480 256c0-63.53-32.06-121.94-85.77-156.24-11.19-7.14-26.03-3.82-33.12 7.46s-3.78 26.21 7.41 33.36C408.27 165.97 432 209.11 432 256s-23.73 90.03-63.48 115.42c-11.19 7.14-14.5 22.07-7.41 33.36 6.51 10.36 21.12 15.14 33.12 7.46C447.94 377.94 480 319.53 480 256zm-141.77-76.87c-11.58-6.33-26.19-2.16-32.61 9.45-6.39 11.61-2.16 26.2 9.45 32.61C327.98 228.28 336 241.63 336 256c0 14.38-8.02 27.72-20.92 34.81-11.61 6.41-15.84 21-9.45 32.61 6.43 11.66 21.05 15.8 32.61 9.45 28.23-15.55 45.77-45 45.77-76.88s-17.54-61.32-45.78-76.86z"></path>
              </svg>
            )}
          </button>
        )}
        {'gui' in this.props && this.props.gui.includes('fullscreen') && (
          <button onClick={this.toggleFullScreen}>
            <svg viewBox="0 0 448 512">
              <path d="M0 180V56c0-13.3 10.7-24 24-24h124c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H64v84c0 6.6-5.4 12-12 12H12c-6.6 0-12-5.4-12-12zM288 44v40c0 6.6 5.4 12 12 12h84v84c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12V56c0-13.3-10.7-24-24-24H300c-6.6 0-12 5.4-12 12zm148 276h-40c-6.6 0-12 5.4-12 12v84h-84c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h124c13.3 0 24-10.7 24-24V332c0-6.6-5.4-12-12-12zM160 468v-40c0-6.6-5.4-12-12-12H64v-84c0-6.6-5.4-12-12-12H12c-6.6 0-12 5.4-12 12v124c0 13.3 10.7 24 24 24h124c6.6 0 12-5.4 12-12z"></path>
            </svg>
          </button>
        )}
        {'gui' in this.props && this.props.gui.includes('largeplay') && (
          <button onClick={this.toggle} className={classNames('fullsize', { 'hide': this.state.status === 'playing' })}>
            <PlayDuotone />
          </button>
        )}
        {this.props.children}
        <video
          ref={c => {
            this.video = c
          }}
          onClick={this.onClick}
          autoPlay={this.props.autoplay}
          muted={this.state.muted}
          playsinline
          loop={this.props.loop}
          onPlay={this.onPlay}
          onPause={this.onPause}
          onStop={this.onStop}
          onEnded={this.onEnded}
          onWaiting={this.onWaiting}
          onLoadedData={this.onLoadedData}
          onTimeUpdate={this.onTimeUpdate}
        >
          {'webm' in this.props && (
            <source src={this.props.webm} type="video/webm" />
          )}
          {'ogv' in this.props && (
            <source src={this.props.ogv} type="video/ogg" />
          )}
          <source src={this.props.mp4} type="video/mp4" />
        </video>
      </div>
    )
  }
}
