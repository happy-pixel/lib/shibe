import style from './style.scss'
import { h, Component } from 'preact'
import { Messenger } from 'models/messenger.js'

let classNames = (require('classnames/bind')).bind(style)

/**
 * iframe z obslugą komunikcji za pomocą PostMessages
 *
 * @class Iframe
 * @extends {Component}
 * @example
 * <Iframe
 *   width="300px"
 *   height="250px"
 *   url="http://example.com"
 *   package={
 *     'event1': 'http://example.com/event1.gif',
 *     'event2': 'http://example.com/event2.gif',
 *     'event3': 'http://example.com/event3.gif'
 *   }
 *   style={
 *    'border': '3px dotted red'
 *   }
 *   onShow=function(){}
 * />
 *
 * @prop {object} props - zestaw właściwości komponentu
 * @prop {string} props.src - adres iframe
 * @prop {string} [props.width] - szerokość komponentu wraz z jednostką
 * @prop {string} [props.height] - wysokość komponentu wraz z jednostką
 * @prop {string} [props.objectFit=contain] - @see https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit
 * @prop {string} [props.validate=never] - @see https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XUL/image#a-validate
 * @prop {object} [props.style] - obiekt ze stylami inline
 * @prop {function} [props.onLoad] - callback wywoływany po załadowaniu się źródła wyrenderowanego iframe
 * @prop {function} [props.onHello] - callback wywoływany po udanym nawiązaniu komunikacji z iframe
 * @prop {function} [props.onShow] - callback wywoływany przez iframe (lub automatycznie, jesli nie uda się nawiązać komunikacji)
 * @prop {function} [props.onHide] - callback wywoływany przez iframe
 */
export class Iframe extends Component {
  constructor (props, state) {
    super(props, state)

    this.state.status = 'initialized'

    this.onHello = this.onHello.bind(this)
    this.onShow = this.onShow.bind(this)
    this.onHide = this.onHide.bind(this)
    this.handleOnLoad = this.handleOnLoad.bind(this)
  }

  /**
   * funkcja wywoływana po nawiązaniu udanej komunikacji z iframką za pomocą PostMessages
   */
  onHello () {
    this.setState({ status: 'contact' })
    if (typeof this.props.package === 'object') this.messenger.send('package', this.props.package)
    if (typeof this.props.onHello === 'function') this.props.onHello()
  }

  /**
   * jeżeli przez 100ms nie uda się nawiązać komunikacji z iframe, onShow jest wywoływane automatycznie
   * w innym przypadku czeka na odpowiednią wiadomość z iframe
   */
  onShow () {
    this.setState({ status: 'shown' })
    if (typeof this.props.onShow === 'function') this.props.onShow()
  }

  /**
   * funkcja wywoływana przez iframe
   */
  onHide () {
    this.setState({ status: 'hidden' })
    if (typeof this.props.onHide === 'function') this.props.onHide()
  }

  componentDidMount () {
    this.messenger = new Messenger({
      source: {
        node: window,
        origin: window.location.origin
      },
      target: {
        node: this.base.contentWindow,
        origin: this.props.src
      }
    })

    let autoShow = setTimeout(() => {
      if (typeof this.props.onShow === 'function') this.props.onShow()
    }, 100)

    this.messenger.on('hello', () => {
      clearTimeout(autoShow)
      this.onHello()
    })
    this.messenger.on('show', () => this.onShow())
    this.messenger.on('hide', () => this.onHide())
  }

  /**
   * obsługa eventu onLoad dla iframe
   */
  handleOnLoad () {
    this.messenger.send('hello')
    this.setState({ status: 'loaded' })
    if (typeof this.props.onLoad === 'function') this.props.onLoad()
  }

  render () {
    return (
      <iframe
        width={ this.props.width }
        height={ this.props.height }
        src={ this.props.src }
        scrolling="no"
        allow="autoplay,accelerometer,fullscreen,geolocation,gyroscope,speaker"
        allowFullScreen
        referrerpolicy="origin-when-cross-origin"
        className={ classNames('iframe') }
        onLoad={ this.handleOnLoad }
      />
    )
  }
}
