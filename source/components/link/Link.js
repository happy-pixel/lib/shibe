import style from './style.scss'
import { h, Component } from 'preact'

let classNames = (require('classnames/bind')).bind(style)

/**
 * prosty link &lt;a/&gt;
 * @class Link
 * @extends {Component}
 * @example
 * <Link href="http://example.com">
 *    <img src="http://placekitten.com/300/250" />
 * </Link>
 *
 * @prop {object} props - zestaw właściwości komponentu
 * @prop {string} props.iframe - adres wyświetlanej strony
 * @prop {string} props.href - adres, na jaki ma przekierowywać link
 * @prop {string} [props.target=_blank] - gdzie ma zostać otwarty link po klinięciu
 * @prop {string[]} [props.classNames] - dodatkowe klasy dla głównego elementu
 * @prop {object} [props.style] - obiekt ze stylami inline
 */
export class Link extends Component {
  constructor (props, state) {
    super(props, state)

    if (!('target' in this.props)) this.props.target = '_blank'
  }

  classNames () {
    return classNames([
      'link',
      this.props.className
    ])
  }

  render () {
    return (
      <a href={this.props.href} target={this.props.target} rel="noopener noreferrer" className={this.classNames()} style={this.props.style}>
        {this.props.children}
      </a>
    )
  }
}
