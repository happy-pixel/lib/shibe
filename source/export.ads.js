import { h, render } from 'preact'
import {
  BannerImage,
  BannerIframe,
  BannerVideo,
  InterstitialImage,
  InterstitialIframe,
  InterstitialVideo
} from './ads/index.js'

if ('shibe' in window === false) {
  window.shibe = {
    Banner: {
      Image: props => render(<BannerImage {...props} />, document.querySelector(props.target)),
      Iframe: props => render(<BannerIframe {...props} />, document.querySelector(props.target)),
      Video: props => render(<BannerVideo {...props} />, document.querySelector(props.target))
    },
    Interstitial: {
      Image: props => render(<InterstitialImage {...props} />, document.body),
      Iframe: props => render(<InterstitialIframe {...props} />, document.body),
      Video: props => render(<InterstitialVideo {...props} />, document.body)
    }
  }
}
