const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = [
  {
    entry: './source/export.ads.js',
    output: {
      filename: 'shibe.min.js',
      path: path.resolve(__dirname, 'build')
    },
    resolve: {
      modules: [
        'node_modules',
        path.resolve(__dirname, 'source')
      ],
      alias: {
        'source': path.resolve(__dirname, 'source')
      }
    },
    module: {
      rules: [
        {
          test: /.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.(s?)css$/,
          use: [
            {
              loader: 'style-loader',
              options: {
                hmr: false,
                singleton: true
              }
            },
            {
              loader: 'css-loader',
              options: {
                modules: true,
                localIdentName: '[hash:base64:6]'
              }
            },
            { loader: 'postcss-loader' },
            { loader: 'sass-loader' }
          ]
        }
      ]
    }
  },
  {
    entry: './source/export.components.js',
    output: {
      filename: 'components.js',
      path: path.resolve(__dirname, 'build')
    },
    resolve: {
      modules: [
        'node_modules',
        path.resolve(__dirname, 'source')
      ],
      alias: {
        'source': path.resolve(__dirname, 'source')
      }
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'components.css'
      })
    ],
    module: {
      rules: [
        {
          test: /.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.(s?)css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader
            },
            {
              loader: 'css-loader',
              options: {
                modules: true,
                localIdentName: '[hash:base64:6]'
              }
            },
            { loader: 'postcss-loader' },
            { loader: 'sass-loader' }
          ]
        }
      ]
    }
  }
]
