(function($) {
    // TODO: make the node ID configurable
    var treeNode = $('#jsdoc-toc-nav');

    // initialize the tree
    treeNode.tree({
        autoEscape: false,
        closedIcon: '&#x21e2;',
        data: [{"label":"<a href=\"Ad.html\">Ad</a>","id":"Ad","children":[]},{"label":"<a href=\"Banner.html\">Banner</a>","id":"Banner","children":[]},{"label":"<a href=\"BannerIframe.html\">BannerIframe</a>","id":"BannerIframe","children":[]},{"label":"<a href=\"BannerImage.html\">BannerImage</a>","id":"BannerImage","children":[]},{"label":"<a href=\"BannerVideo.html\">BannerVideo</a>","id":"BannerVideo","children":[]},{"label":"<a href=\"ChangeOrientation.html\">ChangeOrientation</a>","id":"ChangeOrientation","children":[]},{"label":"<a href=\"Header.html\">Header</a>","id":"Header","children":[]},{"label":"<a href=\"Iframe.html\">Iframe</a>","id":"Iframe","children":[]},{"label":"<a href=\"Image.html\">Image</a>","id":"Image","children":[]},{"label":"<a href=\"Interstitial.html\">Interstitial</a>","id":"Interstitial","children":[]},{"label":"<a href=\"InterstitialIframe.html\">InterstitialIframe</a>","id":"InterstitialIframe","children":[]},{"label":"<a href=\"InterstitialImage.html\">InterstitialImage</a>","id":"InterstitialImage","children":[]},{"label":"<a href=\"InterstitialVideo.html\">InterstitialVideo</a>","id":"InterstitialVideo","children":[]},{"label":"<a href=\"Link.html\">Link</a>","id":"Link","children":[]},{"label":"<a href=\"Video.html\">Video</a>","id":"Video","children":[]}],
        openedIcon: ' &#x21e3;',
        saveState: true,
        useContextMenu: false
    });

    // add event handlers
    // TODO
})(jQuery);
